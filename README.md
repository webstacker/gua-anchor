# Introduction

Logs an event with Google Universal Analytics when an external, download, or non http(s), link is clicked.

Supports IE6+, Firefox, Chrome, Safari, Opera, and any other standards based browser.

Does not track links that the user opens with the right-click context menu e.g. "Open Link in New Tab". 

If present, does not currently replicate target="_blank" when opening links.

## Running tests

1. Open the hosts file (C:\Windows\System32\drivers\etc) and add the following entry "127.0.0.1 test.co.uk"
2. Tests are written using Jasmine and run via karma on Node.js, install [Node.js and NPM](http://nodejs.org/)
3. Navigate to the gua-anchor directory via the terminal and run the command "npm install -g karma-cli"
4. Run the command "npm install"
5. Run "karma start" to execute the tests in phantomjs

To run the tests in real browsers installed on the same machine. While karma is still running (karma start) open the required browser and navigate to "http://test.co.uk:9876/", the terminal will now show results for these browsers.

[VirtualBox can also be used to test other OS/browser combinations](http://blog.reybango.com/2013/02/04/making-internet-explorer-testing-easier-with-new-ie-vms/). Once VirtualBox and the required browsers have been configured, add the following entry to the guest OS hosts file "10.0.2.2 test.co.uk", 10.0.2.2 points to the host machine's localhost where karma is running. As with the host machine, navigate to "http://test.co.uk:9876/" to capture the test results, these will now also be available in the terminal.

## Terminal

For a nicer terminal/console on windows, [Cmder](http://bliker.github.io/cmder/) is highly recommended.