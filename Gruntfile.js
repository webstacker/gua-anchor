module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            build: {
                src: 'js/gua-anchor.js',
                dest: 'js/gua-anchor.min.js'
            }
        },

        watch: {
            scripts: {
                files: ['js/gua-anchor.js'],
                tasks: ['uglify'],
                options: {
                    spawn: false,
                }
            }
        }

        /*
        yuidoc: {
            compile: {
                name: 'package name',
                description: 'package description',
                version: 'package version',
                url: 'package url',
                options: {
                    paths: 'src/',
                    //themedir: 'path/to/custom/theme/',
                    outdir: 'docs/'
                }
            }
        }
        */

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-contrib-yuidoc');

    grunt.registerTask('default', ['watch']);
};
