function testEachInput(testCases, spec) {
    var testName,
        test,
        _it,
        i,
        lenI;

    for (i = 0, lenI = testCases.length; i < lenI; i++) {

        _it = it;
        test = testCases[i];

        //should we ignore this test
        //if (testName.indexOf('xit ') === 0) {
        //    testName = testName.slice(4);
        //    _it = xit;
        //}

        spec(test, _it);
    }
}

//xbrowser get iframe window object, mainly IE<=7
//http://www.nczonline.net/blog/2009/09/15/iframes-onload-and-documentdomain/

function getIframeWindow(iframeElement) {
    return iframeElement.contentWindow || iframeElement.contentDocument.parentWindow;
}

//xbrowser script onload
//http://www.nczonline.net/blog/2009/09/15/iframes-onload-and-documentdomain/

function loadScript(document, url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState) { //IE
        script.onreadystatechange = function() {
            if (script.readyState == "loaded" ||
                script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        }
    } else { //Others
        script.onload = function() {
            callback();
        }
    }

    script.src = url;
    document.body.appendChild(script);
}


//xbrowser iframe onload:
//http://www.nczonline.net/blog/2009/09/15/iframes-onload-and-documentdomain/

function loadIframe(document, url, callback) {
    var iframe = document.createElement("iframe");
    iframe.src = url;

    function handler(ev) {
        callback('srcElement' in ev ? ev.srcElement : ev.target, handler);
    }

    //xbrowser iframe onload:
    //http://www.nczonline.net/blog/2009/09/15/iframes-onload-and-documentdomain/
    if (iframe.attachEvent) {
        iframe.attachEvent("onload", handler);
    } else {
        iframe.onload = handler;
    }

    document.body.appendChild(iframe);

    //return created iframe
    //return reference to handler for removal purposes as it wraps the passed in callback
    return {
        iframe: iframe,
        handler: handler
    };
}


function replaceIframeOnload(iframe, currentHandler, newHandler) {
    if (iframe.attachEvent) {
        iframe.detachEvent('onload', currentHandler);

        iframe.attachEvent("onload", newHandler);
    } else {
        iframe.onload = newHandler;
    }
}

function removeIframeOnload(iframe, currentHandler) {
    if (iframe.attachEvent) {
        iframe.detachEvent('onload', currentHandler);
    } else {
        iframe.onload = null;
    }
}