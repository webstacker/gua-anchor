describe('guaTrackLinks', function() {

    var mockWindow,
        sandbox,
        $;

    beforeEach(function(done) {

        mockWindow = {
            ga: function(send, event, category, type, href, int, obj) {

                obj.hitCallback();

            },
            location: {
                href: ''
            }
        };


        function iframeOnload(iframe, handler) {
            var iframeWindow = getIframeWindow(iframe),
                iframeDocument = iframeWindow.document;

            //inject js dependencies in to sandbox
            loadScript(iframeDocument, '/base/tests/fixtures/lib/jquery.min.js', function() {
                loadScript(iframeDocument, '/base/tests/fixtures/lib/jquery.simulate.js', function() {

                    $ = iframeWindow.jQuery;

                    //mockWindow will have the iframe as its document to interact with
                    mockWindow.document = iframeDocument;

                    spyOn(mockWindow, 'ga').and.callThrough();

                    guaTrackLinks("test.co.uk", mockWindow);

                    done();
                });
            });

            //when an internal anchor simulates a click and loads a new page we don't want the onload event to fire again 
            removeIframeOnload(iframe, handler);
        }

        //create an iframe sandbox for each test
        sandbox = loadIframe(document, "/base/tests/fixtures/sandbox.html", iframeOnload);
    });


    afterEach(function() {
        //remove iframe sandbox
        sandbox.iframe.parentNode.removeChild(sandbox.iframe);
    });


    describe('Given the current domain is http://test.co.uk', function() {

        describe('Internal links', function() {

            var testCases = [{
                i: '/base/tests/fixtures/one/destination.html'
            }, {
                i: './destination.html'
            }, {
                i: '../destination.html'
            }, {
                i: '../../destination.html'
            }, {
                i: 'destination.html?file=page.pdf'
            }, {
                i: 'destination.html'
            }, {
                i: 'four/destination.html'
            }, {
                i: 'http://test.co.uk:9876/base/tests/fixtures/one/two/three/destination.html'
            }];

            testEachInput(testCases, function(testCase, it) {

                it("should ignore internal link '" + testCase.i + "'", function() {

                    $('<a/>', {
                        href: testCase.i,
                        text: testCase.i
                    }).appendTo('body').simulate('click');

                    expect(mockWindow.ga).not.toHaveBeenCalled();
                    expect(mockWindow.location.href).toBe('');

                });

            });
        });


        describe('Internal links (downloads)', function() {

            var testCases = [{
                i: 'google.co.uk/file.pdf?sss=111&rrr=444'
            }, {
                i: 'internallink.pdf'
            }, {
                i: 'subdirectory/internallink.doc'
            }, {
                i: '/internallink.docx'
            }, {
                i: './internallink.mp4'
            }, {
                i: '../internallink.zip'
            }, {
                i: '../../internallink.xls'
            }, {
                i: 'http://test.co.uk/internallink.xlsx'
            }, {
                i: 'http://test.co.uk/subdirectory/internallink.ppt'
            }];

            testEachInput(testCases, function(testCase, it) {

                it("should send a 'download' event to Google Analytics for link '" + testCase.i + "'", function() {

                    var anchor = $('<a/>', {
                        href: testCase.i,
                        text: testCase.i
                    }).appendTo('body'),
                    href = anchor.prop('href');

                    anchor.simulate('click');

                    expect(mockWindow.ga).toHaveBeenCalledWith('send', 'event', 'download', 'click', href, 0, jasmine.any(Object));
                    expect(mockWindow.location.href).toBe(href);

                });

            });

        });



        describe('External links', function() {

            var testCases = [{
                i: 'http://www.google.co.uk?referrer=test.co.uk&b=1'
            }, {
                i: 'HtTp://WwW.bBc.Co.Uk/news/'
            }, {
                i: 'https://www.google.com/doodles/finder/2014/All%20doodles'
            }, {
                i: 'https://meta.cooking.stackexchange.com/somedir/anotherdir/externallink1.html?sss=111&rrr=444'
            }, {
                i: 'http://meta.cooking.stackexchange.com'
            }, {
                i: 'http://www.amazon.co.uk/gp/product/1849906637'
            }, {
                i: 'http://www.ada.gov/hospcombrprt.pdf'
            }, {
                i: '//www.google.co.uk?referrer=test.co.uk&b=1'
            }, {
                i: '//WwW.bBc.Co.Uk/news/'
            }, {
                i: '//www.google.com/doodles/finder/2014/All%20doodles'
            }, {
                i: '//meta.cooking.stackexchange.com/somedir/anotherdir/externallink1.html?sss=111&rrr=444'
            }, {
                i: '//meta.cooking.stackexchange.com'
            }, {
                i: '//www.amazon.co.uk/gp/product/1849906637'
            }, {
                i: '//www.ada.gov/hospcombrprt.pdf'
            }];

            testEachInput(testCases, function(testCase, it) {

                it("should navigate to, and send an 'external' event to Google Analytics for '" + testCase.i + "'", function() {

                    var anchor = $('<a/>', {
                            href: testCase.i,
                            text: testCase.i
                        }).appendTo('body'),
                        href = anchor.prop('href');

                    anchor.simulate('click');

                    expect(mockWindow.ga).toHaveBeenCalledWith('send', 'event', 'external', 'click', href, 0, jasmine.any(Object));
                    expect(mockWindow.location.href).toBe(href);
                    
                });

            });


            it('should send an external link event to Google Analytics given image link', function() {

                $('<a/>', {
                    href: 'http://www.google.co.uk',
                    html: '<img src="http://cdn.sstatic.net/cookingmeta/img/logo.png?v=12cf670383d6" alt="Image link" />'
                }).appendTo('body').children('img').simulate('click');

                expect(mockWindow.ga).toHaveBeenCalledWith('send', 'event', 'external', 'click', 'http://www.google.co.uk/', 0, jasmine.any(Object));
                expect(mockWindow.location.href).toBe('http://www.google.co.uk/');

            });


            it('should send an external link event to Google Analytics given block link', function() {

                $('<a/>', {
                    href: 'http://www.google.co.uk',
                    html: '<h2>Block link</h2><p><span>bla bla <b>bla</b></span></p>'
                }).appendTo('body').find('p span b').simulate('click');

                expect(mockWindow.ga).toHaveBeenCalledWith('send', 'event', 'external', 'click', 'http://www.google.co.uk/', 0, jasmine.any(Object));
                expect(mockWindow.location.href).toBe('http://www.google.co.uk/');

            });

        });


        describe('Non http(s) links', function() {

            var testCases = [{
                i: 'mailto:someone@test.co.uk'
            }, {
                i: 'mailto:someone@someplace.co.uk'
            }, {
                i: 'tel:11112223333'
            }, {
                i: 'sms:44444555666'
            }, {
                i: 'facetime:john@google.co.uk'
            }, {
                i: 'ftp://user001:secretpassword@private.ftp-servers.example.com/mydirectory/myfile.txt'
            }, {
                i: 'javascript:alert("Hello");'
            }];

            testEachInput(testCases, function(testCase, it) {

                it("should send a 'scheme event' (mailto:, tel:, etc) to Google Analytics for '" + testCase.i + "'", function() {

                    //create anchor
                    var anchor = $('<a/>', {
                            href: testCase.i,
                            text: testCase.i
                        }).appendTo('body'),
                        href = anchor.prop('href'),
                        scheme = href.slice(0, href.indexOf(':') + 1);

                    anchor.simulate('click');
                    
                    expect(mockWindow.ga).toHaveBeenCalledWith('send', 'event', scheme, 'click', href, 0, jasmine.any(Object));
                    expect(mockWindow.location.href).toBe(href);

                });

            });

        });

    });
});

//hack to get the reporter to display the describe blocks.
//first run is ok but subsequent runs omit the descriptions

describe('', function() {
    xit('', function() {});
});
